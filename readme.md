## Installation

Installation configurations with terminal;

-   composer update
-   php artisan migrate
-   php artisan db:seed
-   php artisan passport:keys
-   php artisan passport:client --personal (EnterName: PublNews)

Run with ip address on localhost: php artisan serve --host 192.168._._
